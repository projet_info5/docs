# Documentation Docker
###### Dernière édition : 25 Mars 2020


Cette documentation [Docker][] n'a pas pour but d'être exhaustive, elle présente juste les notions nécessaires à la compréhension de Docker en général.

[Portail de documentation Docker](https://docs.docker.com/)

[Installation sous Ubuntu](https://docs.docker.com/install/linux/docker-ce)

### DOCKER, CONTENEURS, IMAGES

Le projet [Docker][] a pour but d'exécuter des programmes de manière cloisonnée, dans un environnement séparé du reste de la machine (un peu comme une machine virtuelle).
La conception même de Docker repose sur l'utilisation du noyau Linux qui va permettre de réaliser ce cloisonnement.
Au final, un conteneur Docker contiendra son propre système de fichier, semblable à celui de Linux, mais contenant uniquement les ressources qu'on lui aura demandé d'installer.

Une _image_ Docker est créée à partir d'un Dockerfile. La création d'un Dockerfile est expliqué dans la [partie suivante](docker.md#dockerfile). Une image Docker est comparable à une image disque _.iso_ , dans le sens ou il s'agit juste d'en enregistrement, rien n'est encore exécuté.

Pour créer un _conteneur_ Docker, c'est-à-dire l'instance d'une image il suffit d'utiliser la commande 
    
    docker create <image>
    
Celle-ci aura pour effet de créer un conteneur basé sur l'image indiquée, comme expliqué dans la partie sur les [conteneurs](docker.md#manipulation-des-conteneurs).

Pour faire un parallèle avec la programmation orientée objet, l'image correspondrait à une classe, et le conteneur à un objet. 
L'idée étant que l'on peut créer de multiples conteneurs basés sur la même image, par exemple pour créer de la redondance.
Un autre avantage est bien sur que l'environnement d'exécution est le même pour chaque conteneur ce qui assure une certaine portabilité.


### DOCKERFILE

Un Dockerfile s'apparente à une recette de cuisine. C'est en fait une suite de commande à exécuter pour atteindre un état souhaité. 
Par exemple, lorsqu'on installe un système d'exploitation sur une machine, on va rajouter des logiciels pour pouvoir arriver à notre outil de travail.
En général, on part d'une image disponible mise à disposition sur le [Docker Hub](https://hub.docker.com/search?q=&type=image), c'est une grande bibliothèque mettant à disposition des images de toutes sortes.
On retrouve notamment les images [ubuntu](https://hub.docker.com/_/ubuntu), [node](https://hub.docker.com/_/node) 
et également l'image utilisée pour le cours de Programmation Fonctionnel/OCaml 
et Langages&Traducteurs/Coq [ricm-pf-tools](https://hub.docker.com/r/jahierwan/ricm-pf-tools).
Presque toutes les images sont basées sur d'autres, ainsi, l'image _node_ repose elle-même sur l'image _ubuntu_.

Une fois un Dockerfile terminé, on doit créer notre image, on utilise la commande suivante :

    docker build -t <image> .
    
Le point à la fin indique à Docker qu'il doit chercher le Dockerfile dans le répertoire courant. Les noms valides sont (non-exhaustif) Dockerfile, dockerfile, DOCKERFILE

_image_ est le nom que vous souhaitez donner à votre image.

Pour lister les images disponibles sur votre machine, utilisez l'une des commandes suivantes:
    
    docker image list
    docker images

Docker est doté d'un système de cache très puissant. Si deux images sont basées sur la même image mère, cette dernière ne sera téléchargée et stockée qu'en un seul exemplaire sur votre machine.

Pour supprimer une image, on utilise

    docker rmi <image>

##### EXEMPLE

L'une des applications les plus facile à _dockeriser_ sont celles basées sur [node.js](https://nodejs.org).

Voici un exemple de Dockerfile pour application _node.js_ fortement commenté.

    # On se base sur l'image du Docker Hub "node" en version 10
    FROM node:10
    
    # On spécifie le "WORKDIR", ce sera notre répertoire de base à l'intérieur du conteneur
    WORKDIR /app
    
    # Dans le cas d'une application node.js, on doit installer les dépendances nécessaires
    # spécifiées dans package.json (avec éventuellement un verouillage des versions dans le package-lock.json)
    
    # On copie donc package.json et éventuellement package-lock.json dans le répertoire courant (à savoir /app, le WORKDIR)
    COPY package*.json ./
    
    # On installe les dépendances avec la commande npm, disponible car nous sommes partis de l'image node. 
    RUN npm install
    
    # On copie le reste des fichiers sources (on aurait pu faire ça en même temps que le package.json, mais cela rend plus difficile le debugging en cas d'échec de création de l'image)
    COPY . .
    
    # Dans notre cas, il s'agit d'une application Web, mettant des ressources à disposition sur le port 80. On doit donc exposer ce port
    EXPOSE 80
    
    # On spécifie le point d'entrée de notre image
    # C'est la commande qui sera exécutée lorsqu'un conteneur basé sur cette image sera créé
    CMD ["npm" , "start"]

On crée ensuite notre image avec la commande
    
    docker build -t myapp-img .
    

### MANIPULATION DES CONTENEURS


Pour créer un conteneur, la commande est la suivante, où _image_ est l'image sur laquelle le conteneur doit être basé.

    docker create <image> # Lire la suite avant de copier SVP

MAIS il est rare qu'on se contente d'utiliser la commande ainsi car beaucoup de paramètres sont disponibles.


##### PARAMETRES

Il est d'usage de placer les paramètres avant le nom de l'image.

Voici quelques paramètres utiles:

+ --name _nom_ 

Permet de donner un nom au conteneur, par défaut Docker utilise des noms générés automatiquement comme _inspiring\_lovelace_ ou encore _phenomenal\_turing_.
En général on utilise la convention <nom_image>_# ou # est le nombre de conteneur correspondant à cette image actuellement en service sur la machine.

+ --publish ou -p PORT_MACHINE:PORT_CONTENEUR

Permet de publier un port exposé. Dans notre exemple de Dockerfile, nous avons exposé le port 80 du conteneur. Il faut maintenant le publier, c'est-à-dire le connecter à un port de notre machine. 
Pour rappel, les ports 0 à 1024 sont protégés sur votre machine par le système d'exploitation Linux.

+ -it ou -i -t ou --interactive --tty

Permet d'attacher la sortie et l'entrée standard du conteneur dans la ligne de commande de votre machine. Pratique pour débugger.


La liste complète des paramètres est visible [ici](https://docs.docker.com/engine/reference/commandline/create/) et également dans le manuel de commande Linux :

    man docker create

##### EXEMPLE

En conservant notre exemple, on peut démarrer proprement un conteneur basé sur _myapp-img_ grâce à la commande suivante:

    docker create --name myapp-img_1 -p 8080:80 myapp-img

Le port 8080 de votre machine est alors connecté au port 80 du conteneur

##### DEMARRAGE

_Last but not least_ il convient de démarrer le conteneur avec la commande

    docker start <conteneur>

Pour afficher tous les conteneurs, en fonctionnement ou arrêtés, utilisez la commande suivante :

    docker ps -a

Pour arrêter un conteneur, c'est également tout simple

    docker stop <conteneur>

Et pour le supprimer

    docker rm <conteneur>

##### ACCES AU CONTENEUR

Pour des motifs de débuggage ou de collecte d'informations, il peut être utile d'accéder au système de fichier du conteneur. On peut alors utiliser la commande _docker exec_ qui permet d'exécuter une commande à l'intérieur du conteneur.

Par exemple :

    docker exec <conteneur> bash
    
permettra d'ouvrir un bash à l'intérieur du conteneur et de naviguer en console, comme sur votre machine ! 
[Il faut cependant que votre image contienne le programme _bash_ ce qui est le cas par défaut pour l'image _ubuntu_]


### docker-compose

Si vous en avez marre de taper de longue commandes pour créer vos conteneurs, intéressez-vous à [docker-compose](https://docs.docker.com/compose/).



### RESSOURCES

[Portail Docker][Docker]

[Docker Hub](https://hub.docker.com/search?q=&type=image)

[Référence : Ligne de commande](https://docs.docker.com/engine/reference/commandline/)

[Référence : Dockerfile](https://docs.docker.com/engine/reference/builder/)

[Docker]: https://www.docker.com/ 