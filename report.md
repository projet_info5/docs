# PROJET STAPS - APPLI TROPS
Polytech Grenoble - Projet INFO5 - 2020


<img src="/assets/logo-uga.svg"  alt="Logo UGA" height="100">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="/assets/logo-polytech.png" alt="Logo Polytech Grenoble">


## Cahier des charges 

Pour toute la durée du proje,t nos clients étaient un groupe d'étudiants en STAPS. Ils ont donc définis le cahier des charges que nous avons suivi.     
L'objectif principal était de développer une application de location de matériel entre particuliers. Les caractéristiques principales de cette application sont les suivantes :  
- Inscription et connexion/deconnexion
- La publication et la gestion (suppression) d'annonce 
- Système de profil qu'on doit pouvoir consulter et modifier pour voir ses propres annonces.
- Système de recherche des annonces les plus pertinentes par différentes caractéristiques comme le prix ou la catégorie.

## Technologies utilisées

### Front End

Pour le front end utilisateur, nous avons utilisé le framework **Flutter** développé par Google. Ce framework permet de développer des applications natives pour Android et iOS. Le langage compatible avec ce framework est le **Dart**, aussi développé par Google. 
Au début de ce projet, nous n'avions jamais utilisé ni Flutter ni Dart, cela nous a donc permis de nous former sur ces technologies.

### Back End

Nous avons utilisé une base de données **MongoDB** et nous avons intégré une API grâce à **Mongoose** et au framework **ExpressJS**, framework utilisant **NodeJS**, technologie permettant d'utiliser **Javascript** comme langage serveur.

## Architecture techniques
<img src="assets/archi2.png">  

L'architecture de notre projet est une architecture RESTful classique. En effet, comme on peut le voir sur le schéma ci-dessus, les clients (utilisateurs de l'application mobile) envoient une requête à l'API qui communique avec la base de données afin de retourner les ressources demandées par l'utilisateur (si ce dernier est autorisé à y accéder).  
Notre architecture suit donc des règles afin d'être le plus en accord avec ce qui se fait dans le monde professionnel:

- **Règle 1** : Notre API se base sur les URI (Uniform Resource Identifier) afin d’identifier une ressource.
    - Liste des utilisateurs : _**api.trops.space/users**_  

* **Règle 2** : Les méthodes de requête HTTP comme identifiant des opérations plutôt que d’inclure l’opération dans l’URI de la ressource.
    - Créer une annonce : _**POST api.trops.space/advert**_ plutôt que _**GET api.trops.space/advert/create**_  

- **Règle 3** : Les requêtes demandant des données sensibles sont envoyées avec un jeton (token) passé en en-tête d'autorisation
    -  En-tête de la récupération des annonces d'un utilisateur précis : _**Authorization : Bearer ey[...]5A**_



## Réalisations techniques

Dans ce projet, notre équipe à réalisé 3 logiciel différents, qui ont tous un objectif différent, mais sont tous indispensable pour répondre aux besoins du client.

### API
Notre projet contient et utilise une API REST qui a entièrement été développée par notre équipe à l'aide de **ExpressJs**.  
Cette API met à disposition divers endpoint en rapport avec les utilisateurs, les annonces, les catégories d’annonces, les favoris afin de permettre aux clients (Application mobile ou panneau administrateur) d'interagir avec la base de données MongoDB, base qui contient toutes les informations utiles du projet (utilisateurs, annonces, favoris, etc...).

### Application mobile
Partie "principale" du projet, l'application mobile est le produit demandé par le client, c'est donc sur cet élément qu'il avait le plus de contrôle, notre rôle étant d'implémenter ce qu'il nous demandait.  

L'application étant la seule ressource disponible aux utilisateurs (pas de version Web lors de l'écriture de ce rapport), cette dernière devait proposer toutes les fonctionnalités permettant donc de répondre au cahier des charges cité précédemment dans le rapport.  
Le premier besoin de l'application, à savoir, être capable de se créer un compte et de se connecter nous a lancé dans le développement de l'API, indispensable au vu du fait que les utilisateurs peuvent accéder aux données de la base de données, mais également les modifier (ajout/mise à jour/suppression).  

Le développement a demandé un certain temps d'adaptation à toute l'équipe, équipe qui n'avait jamais fait de Dart jusqu'à ce projet.
Même si la technologie était nouvelle pour nous, cela ne nous a pas empêché d'arriver à proposer un produit qui répond à toutes les exigences de base du cahier même si certains points ont été un peu plus complexe à gérer comme la gestion des photos.
En effet, une annonce pouvant contenir jusqu'à 4 images, prise par l'utilisateur ou provenant de sa galerie, elles devaient être sauvegardées afin que les autres utilisateurs puissent les voir lorsqu'il consulte les annonces. L'obstacle principal de cette fonctionnalité est que MongoDB ne propose pas de solution d'hébergement d'image gratuite.  

Pour palier ce problème, nous avons décidé de stocker les images dans le même serveur qui stocke actuellement notre API. Un autre problème était la taille des images. En effet, de nos jours, les capteurs des smartphones ayant de plus en plus de mégapixels, les photos résultantes sont de plus en plus lourdes, ce qui affecte grandement les délais de chargement vers le serveur et de téléchargement depuis le serveur.
Nous avons donc décider de compresser les images avant de les envoyer sur le serveur et d'afficher coté client l'image chargée (et donc compressée) pour montrer que le chargement était réussi. Ce choix nous a permis de gagner en espace de stockage sur le serveur, mais également en temps de traitement.  

Pour résumer le chargement d'une image par l'utilisateur lors de la création d'une annonce est coupée en 5 étapes :
- On récupère la photo prises ou choisie par l'utilisateur.
- On compresse l'image en gardant 20% de la qualité d'origine.
- On envoi l'image au serveur via l'API (qui hash le nom de fichier pour éviter les collisions).
- l'API renvoi le lien de l'image sur le serveur.
- On affiche l'image correspondant au lien renvoyé.

Si le chargement vers le serveur est un échec, la sablier disparaît et la photo reste vide, l'utilisateur doit réessayer.
Les documents mongoDB relatifs aux annonces contiennent donc les liens des images sur le serveur, cela résout donc notre problème.

<img src="assets/gestionImage.png">  


Suite à une première version fournie aux clients qui contenait les fonctionnalités les plus importantes (inscription/connexion, publication d'annonce), le client a voulu ajouter des éléments dans certains aspects du produit :
- Les annonces doivent pouvoir être éditées.
- L'utilisateur doit pouvoir ajouter des annonces à ses favoris pour les voir dans son profil.
- Des "pubs" doivent pouvoir être affichées au milieu des annonces.
- Les annonces doivent pouvoir être recherchées par lieu.


Ses nouvelles fonctionnalités n'ont pas posé de gros problèmes de développement, elles ont uniquement nécessité l'ajout d'endpoints dans l'API et la modification des champs dans les documents mongoDB (pour les lieux et les favoris par exemple)

### Panneau administrateur 
L'interface administrative a pour objectif de permettre aux propriétaires du projet de gérer les données sans être obligés de passer par l'application.  
En effet, l'application n'a pas été créée dans le but de gérer des données, mais de les visualiser et de les exploités par des personnes externes au projet, ce qui entre en contradiction avec le besoin des administrateurs. De plus, une application mobile n'est pas forcément la solution la plus utile pour un gestionnaire de données.  

Cette interface permet donc le contrôle de trois éléments : les utilisateurs, les catégories et les annonces.Il est donc possible de modifier, voir ou supprimer rapidement une annonce qui ne respecterait pas les règles de l’application. L’authentification administrative est réalisée de la même manière que pour les utilisateurs de l’application, mais les données sont stockés dans un tableau différent, c’est-à-dire que les données utilisateurs et administrateurs sont totalement séparées pour des raisons de sécurité.  

L’adresse de l'interface ne doit donc être connue que des administrateur du projet, mais comme dit précédemment, même si un utilisateur trouve l'adresse, il doit encore passer l'authentification qui est totalement indépendante du système d'utilisateurs de l'application.




## Gestion de projet
### Equipe
Notre équipe était composée de 4 personnes dont les rôles étaient différents.  

Les 2 principales parties du développement sont évidemment le Front-End et le Back-End qui sont eux-mêmes composés de plusieurs éléments. Chaque partie sera détaillée avec les personnes ayant travaillé dessus afin de bien découper le projet. 

- Front-End :
    - Design (Tanguy)
    - Développement Dart (Ariane, Mathieu, Quentin, Tanguy)

- Back-End :
    - Gestion du serveur (Mathieu, Tanguy)
    - Gestion base de données (Mathieu, Quentin, Tanguy)
    - Développement API REST (Quentin, Tanguy)

En plus de ses 2 parties qui concernent le développement de l'application, Mathieu Vincent a occupé le rôle de chef de projet. C'est lui qui prenait contact avec les STAPS et qui organisait les rencontres avec le client dans le but de discuter de l'avancement du projet, mais également pour planifier les ajouts et modifications à apporter.

### Méthode
Cette sous-partie, décrira les moyens mis en œuvre pour lancer le projet (à partir de zéro) et le faire avancer jusqu'à aujourd'hui.  
La méthode que nous avons décidé d'utiliser était une méthode "Agile". En effet, même si cette méthodologie n'est pas maîtrisée par notre groupe en raison de notre manque d'expérience professionnelle, nous avons essayé de mettre en œuvre certains de ses aspects autant que possible.

- Au début, le projet a été divisé en parties importantes (les principales parties du projet).

- Une fois que les objectifs de ses parties définis à la suite d'un brainstorming collectif, nous avons divisé ses grandes parties en parties plus petites et ainsi de suite jusqu'à obtenir des tâches équivalentes à des sprints.

- Nous avons créé une fiche d'objectifs pour savoir quel travail doit être effectué et comment il doit l'être distribués au sein du groupe.

- Enfin, chaque jour, un "daily meeting" était organisé pour savoir où en était chaque membre de l'équipe, et ce, sur quoi on pouvait travailler pour cette nouvelle journée. Ces réunions ont permis de maintenir le groupe informé des tâches actuelles de chacun et, en cas de blocage, le groupe pouvait s'entraider plus facilement.


### Planning
Pour gérer au mieux notre temps, nous avons décidé de réaliser un diagramme de Gantt. Ce diagramme nous a servi de référence tout au long du projet bien qu'il n'ait pas été respecté à la lettre.   
<img src="/assets/planning.png"  alt="Panning" height="400">

### Gestion des risques
Plusieurs risques ont été identifiés au début du projet. Ces risques, plus ou moins modérés, pourraient compromettre le bon déroulement du projet. C'est pourquoi nous avons trouvé différentes solutions pour les réduire ou même, les supprimer.   
   
Le risque majeur était le manque de compétences. Nous ne connaissions pas la technologie que nous voulions utiliser. Nous avons donc pris une semaine en début de projet pour nous former sur cette nouvelle technologie. Lors de cette semaine, nous avons suivi plusieurs tutoriels sur le site officiel du Framework Flutter. Cette semaine de formation nous a permis d'être prêts pour le projet.    
   
Un autre risque très important pour le projet était la mauvaise estimation des délais de développement. Pour pallier à ce problème, nous avons réalisé un diagramme de Gantt avec beaucoup de marge dans les délais afin de s’assurer d’avoir du temps. De plus, nous avons réservé une semaine de marge en fin de projet pour permettre des débordements.   
   
Le dernier risque était de ne pas avoir de matériel assez puissant pour pouvoir développer et simuler l'application. Nous avons utilisé nos smartphones afin de réduire au maximum la charge sur nos ordinateurs. En effet, Android Studio demande beaucoup de RAM pour fonctionner correctement.   


## Outils

Tout au long du développement de notre projet, divers outils ont été utilisés dans des buts différents. Cette section aura pour but de les nommer et d'expliquer le rôle des outils qui nous ont permis de réaliser notre projet.

### Outils de développement
Ce sont les éléments principaux de notre projet. En effet, sans eux, aucune ligne de code n'aurait pû être écrite.
Nous avons principalement utilisé 2 IDE, un pour le Front-End, l'autre pour le Back-End.  

- **Android Studio** : Nous avons choisi Android Studio,environnement de développement Android et développé par Google car ce dernier est gratuit, facile à utiliser mais surtout car il permet d'installer l'environnement de développement Flutter très facilement et rapidement du fait que Flutter est également un produit Google.

- **Visual Studio Code** : Pour le développement de l'API, qui ne nécessitait pas l'environnement Flutter, nous nous somme tourné vers l'IDE gratuit de Microsoft. Ce dernier étant disponible sur tous les principaux OS (Windows, Linux, MacOS) et ne demandant pas beaucoup de ressources pour fonctionner, il était le choix le plus logique. 

### Outils de gestion de version

Notre projet devant être hébergé pour contrôler son ́evolution et son déploiement, nous avons utilisé GitLab, plateforme d’hébergement de projets GIT.  

Le choix de **GitLab** à été fait du fait que l'école l'utilise pour stocker les projets élèves, mais également pour des fonctionnalités additionnelles qu’il propose.  

En effet, en plus d’héberger notre projet, nous avons pu effectuer la totalité du contrôle d’avancée directement sur GitLab. L’avancement a pu être contrôlé grâce à l’option ”issues” de GitLab qui permet la mise en place d’un backlog avec assignation de tâches.


### Outils de tests

Lors du développement de l'API, il était important de pouvoir tester les endpoints sans passer par l'application.  
C'est pourquoi nous avons utilisé le logiciel **Postman**, qui permet de construire et d’exécuter des requêtes HTTP et de les stocker dans un historique afin de pouvoir les rejouer. Cet outil est très utile pour tester les requêtes vers notre API afin de contrôler le retour de ses dernières et corriger les problèmes ́eventuels les concernant.

### Outils de collaboration

L’équipe de notre projet était scindée en 2 groupes distincts, le groupe de développement au sein de Polytech et le groupe de “client” des STAPS. La communication était donc au centre du processus de création et représentait un défi non-négligeable entre les différentes parties. En effet, d’un côté, les STAPS avaient une vision du produit souhaité et de l’autre, l’équipe de développement percevait les défis techniques auquel elle allait devoir faire face pour arriver à fournir le travail attendu.  

Tout le défi était, d’arriver à communiquer étant donné la distance des deux groupes, mais surtout de communiquer de manière à ce que les deux parties se comprennent. En effet, l’équipe de développement ne pouvait communiquer avec les STAPS comme elle le faisait en son sein, les termes et notions techniques n'étant pas connus du client, elle devait donc s’adapter.  

Afin de répondre aux deux problèmes cités précédemment, notre équipe a donc utilisé deux moyens de communications distincts, à savoir, **Slack** et **Whatsapp**, afin de laisser le technique à l’équipe de développement.  

Un groupe WhatsApp a été créé avec tous les membres des deux équipes. Cette conversation nous a notamment permis de pouvoir fixer les points et heures de rendez-vous pour nos réunions. Si jamais nous faisions face des problèmes ou questions urgentes, nous pouvions directement les contacter sans introduire de délai supplémentaire. De même, il leur est arrivé de nous contacter par ce biais pour nous communiquer des informations sur ce qu’ils voulaient ou nous demander des précisions sur un aspect du projet.  

Pour la communication interne de notre équipe, nous utilisions plutôt Slack. Cela a rendu la communication plus fluide en nous permettant d’organiser les sujets de conversation et de bien les séparer pour gérer au mieux les échanges. Slack a également été utilisé afin de partager des ressources techniques entre les membres de l'équipe de développement de Polytech. En effet, travaillant sans cesse ensemble, au même endroit et en équipe réduite, la communication n’a pas été un problème

Ces outils se sont avérés particulièrement utiles lors de la dernière semaine du projet en raison du confinement, nous permettant d’avancer malgré le fait que chaque membre du groupe était à un endroit différent des autres.



### Outils de documentation

Un projet informatique étant généralement conséquent et complexe à son terme, il est nécessaire de le documenter afin que quelqu'un le reprenant ou étant affecté au développement de ce dernier sans y avoir contribué depuis le début puisse comprendre et s'adapter dans les meilleures conditions possibles.  
Notre projet utilisant une API, codée par notre équipe, nous étions dans l'obligation de documenter cette dernière dans l'optique de reprise du projet plus tard ou même par d'autres étudiants dans les années à venir. Afin de réaliser cette tâche de documentation tout en essayant de rendre le résultat le moins repoussant possible, nous avons utilisé l'outil **Swagger**.  

L'éditeur Swagger nous permet de générer de la documentation pour notre API Web en écrivant un fichier YAML qui permet de détailler, pour chaque endpoint, les methodes de requetes HTTP, le code et les données retournées, etc... tout en organisant ses informations via un design simple mais agréable à l'oeil.  

La documentation Swagger pour notre API est disponible ici : [**Documentation SWAGGER**](https://gitlab.com/projet_info5/trops_api/-/blob/master/swagger.yaml)


## Métriques logicielles

### Répartition des commits

Front End :  
Tanguy : 29%  
Quentin : 25%  
Ariane : 23%  
Mathieu : 23% 
On peut voir que les commits pour le front sont à peu près également répartis car nous avons travailler de manière égale sur le développement en flutter de l'application.  
  
Back End :  
Tanguy : 54%   
Quentin : 25%  
Mathieu : 21%  
Pour le back, les commits sont répartis uniquement entre Tanguy, Mathieu et Quentin car ce sont eux qui ce sont principalement occupé du back end.

### Temps ingénieur

Afin d’avoir un ordre d’idée du coût du projet, dans le domaine de l’humain uniquement, nous avons calculé le coût humain total de ce dernier.   

En nous basant sur le salaire moyen brut annuel d’un ingénieur front-end avec 0 année d'expérience (ce qui correspond à l’entièreté de notre équipe) et en ajoutant les différentes charges, un ingénieur coûterait 53 650€ par an à l’entreprise qui l’engage ou 147€ par jour.    

Le projet, lui, n’a pas duré 1 an mais 8 semaines de 35 heures soit 280 heures par personne et 1120 heures au total.   
Ce nombre d’heures représente 160 jours de travail.   

À raison de 147€ par jour, il ne nous reste qu’à multiplié par 160 pour arriver à 23 040€ juste pour le coût humain.   


## Conclusion

### Etat actuelle de l'application

Dans l'état actuel des choses, notre projet n'est pas complètement terminé, bien qu'un nombre significatif de fonctionnalités aient pu être réalisées.  
Nous pouvons citer de manière non-exhaustive :

- La création d'un compte

- La connexion au compte

- La création d'une annonce complète (upload de photos possible)

- Modifier une annonce

- Supprimer une annonce

- Ajouter/Supprimer une annonce des favoris

- Consulter ses annonces dans l'onglet profil

- Consulter ses favoris dans l'onglet profil

- Consultation des dernières offres postées

- Rechercher une offre par mot-clé

- Trier les annonces par pertinance

- Contacter le créateur de l'annonce si le téléphone est renseigné

- Les administrateurs peuvent gérer la base de données via un site web indépendamment de l'application mobile

### Extensions possibles
Différentes améliorations sont possibles. L'état actuel de l'application permet une mise en relation entre les utilisateurs, mais ne permet pas de gérer les réservations directement en son sein. Nous pensons que ce serait l'un des ajouts le plus intéressants afin de faciliter la vie aux utilisateurs, ce qui leur permettrait de s'affranchir d'une étape en plus dans leur gestion de matériel.  

Une autre amélioration possible serait de proposer une version WEB du produit. En effet, de nos jours, les plus grands sites marchands possèdent leur site web et leur application mobile. Même si le mobile est le plus gros marché de nos jours, les commandes WEB restent une source non-négligeable d'utilisateurs.  

Enfin, établir une charte graphique qui rendrait l'application réellement unique en terme de design et de choix esthétiques. En effet, pour le moment, le thème utilisé est celui proposer par Flutter.

### Retour d'expérience
Ce projet a été bénéfique sur de nombreux points.
Tout d'abord en terme de connaissances, ayant utilisé des langages sur lesquels nous n'avions aucune expérience comme le Dart est un plus important dans le secteur dans lequel nous travaillons, consolidant notre expérience dans le domaine des langages haut niveau.  

Le fait également de travailler dans un contexte Agile nous a aidés à nous faire une véritable idée sur ce procédé, ce qui est un atout pour le futur, car en ayant fait l’expérience dès maintenant, cela ne pourra que nous être bénéfique par la suite.  

Le principal bénéfice de ce projet a été la réalisation entière d'une API REST. En effet, pendant nos études et les stages que nous avons effectué, les tâches qui nous sont confiées sont généralement dans le domaine du Front-End. Développer notre propre API nous a donc immergé dans le monde du Back-End et nous a permis de consolider nos compétences et notre savoir dans le domaine. 

## Bibliographie

https://flutter.dev/   
https://medium.com/tag/flutter   
https://jwt.io/   
https://mongoosejs.com/docs/   
https://expressjs.com/fr/starter/installing.html



## Glossaire

JWT : JSON Web Token, standard qui permet l'échange sécurisé de jetons avec une vérification de l'intégrité des données   
API : Application Programming Interface, service qui permet la communication entre deux applications, ici notre application mobile et notre base de données   
VPS : Serveur permettant le partitionnement en plusieurs serveurs indépendants   
Flutter : Framework permettant la réalisation d'application web et mobile native utilisant le langage Dart   
Dart : Langage développé par Google permettant la réalisation d'application web et mobile   
MongoDB : Système de gestion de base de donnéees   
NodeJS : Logiciel de gestion de serveur en JavaScript   
Mongoose : Logiciel permetant de faire le lien entre le serveur NodeJS et la base de données MongoDB   
